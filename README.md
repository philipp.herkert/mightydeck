# AnkiDeck MightyDeck Card Type

## Requirements
- Multiple occlusions on same card
- Enhanced Element uncovering
- Allow the usage of Hints
- Allow Pictures to be enclosed

## TODO 
- Create Git
- Create Development environment
- Rewrite entire Code
- Implement featurs

## Samples
### No Hints - Front
#### {Text}
```
I need to learn this list:
1. {{c1::[[Answer1|c1]]}}
2. {{c2::[[Answer2|c2]]}}
3. {{c3::[[Answer3|c3]]}}
4. {{c4::[[Answer4|c4]]}}
```
```html
<div>I need to learn this list:</div><div>1. {{c1::[[Answer1|c1]]}}<div>2. {{c2::[[Answer2|c2]]}}</div></div><div>3. {{c3::[[Answer3|c3]]}}</div><div>4. {{c4::[[Answer4|c4]]}}</div>
```
#### {cloze:Text}
```
I need to learn this list:
1. [[Answer1|c1]]
2. [...]
3. [[Answer3|c3]]
4. [[Answer4|c4]]
```
```html
<div>I need to learn this list:</div><div>1. [[Answer1|c1]]<div>2. <span class=cloze>[...]</span></div></div><div>3. [[Answer3|c3]]</div><div>4. [[Answer4|c4]]</div>
```

### No Hints - Back
#### {Text}
```
I need to learn this list:
1. {{c1::[[Answer1|c1]]}}
2. {{c2::[[Answer2|c2]]}}
3. {{c3::[[Answer3|c3]]}}
4. {{c4::[[Answer4|c4]]}}
```
```html
<div>I need to learn this list:</div><div>1. {{c1::[[Answer1|c1]]}}<div>2. {{c2::[[Answer2|c2]]}}</div></div><div>3. {{c3::[[Answer3|c3]]}}</div><div>4. {{c4::[[Answer4|c4]]}}</div>
```
#### {cloze:Text}
```
I need to learn this list:
1. [[Answer1|c1]]
2. [[Answer2|c2]]
3. [[Answer3|c3]]
4. [[Answer4|c4]]
```
```html
<div>I need to learn this list:</div><div>1. [[Answer1|c1]]<div>2. <span class=cloze>[[Answer2|c2]]</span></div></div><div>3. [[Answer3|c3]]</div><div>4. [[Answer4|c4]]</div>
```

### Hints - Front
#### {Text}
```
I need to learn this list with Hints:
1. {{c1::[[Answer1|c1]]::Hint1}}
2. {{c2::[[Answer2|c2]]::Hint2}}
3. {{c3::[[Answer3|c3]]::Hint3}}
4. {{c4::[[Answer4|c4]]::Hint4}}
```
```html
<div>I need to learn this list with Hints:</div><div>1. {{c1::[[Answer1|c1]]::Hint1}}<div>2. {{c2::[[Answer2|c2]]::Hint2}}</div></div><div>3. {{c3::[[Answer3|c3]]::Hint3}}</div><div>4. {{c4::[[Answer4|c4]]::Hint4}}</div>
```
#### {cloze:Text}
```
I need to learn this list with Hints:
1. [[Answer1|c1]]
2. [[Answer2|c2]]
3. [[Answer3|c3]]
4. [Hint4]
```
```html
<div>I need to learn this list with Hints:</div><div>1. [[Answer1|c1]]<div>2. [[Answer2|c2]]</div></div><div>3. [[Answer3|c3]]</div><div>4. <span class=cloze>[Hint4]</span></div>
```

### Hints - Back
#### {Text}
```
I need to learn this list with Hints:
1. {{c1::[[Answer1|c1]]::Hint1}}
2. {{c2::[[Answer2|c2]]::Hint2}}
3. {{c3::[[Answer3|c3]]::Hint3}}
4. {{c4::[[Answer4|c4]]::Hint4}}
```
```html
<div>I need to learn this list with Hints:</div><div>1. {{c1::[[Answer1|c1]]::Hint1}}<div>2. {{c2::[[Answer2|c2]]::Hint2}}</div></div><div>3. {{c3::[[Answer3|c3]]::Hint3}}</div><div>4. {{c4::[[Answer4|c4]]::Hint4}}</div>
```
#### {cloze:Text}
```
I need to learn this list with Hints:
1. [[Answer1|c1]]
2. [[Answer2|c2]]
3. [[Answer3|c3]]
4. [[Answer4|c4]]
```
```html
<div>I need to learn this list with Hints:</div><div>1. [[Answer1|c1]]<div>2. [[Answer2|c2]]</div></div><div>3. [[Answer3|c3]]</div><div>4. <span class=cloze>[[Answer4|c4]]</span></div>
```
